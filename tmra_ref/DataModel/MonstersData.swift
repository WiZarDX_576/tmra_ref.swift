//
//  MonstersData.swift
//  tmra_ref
//
//  Created by Vitaly on 26.12.2018.
//  Copyright © 2018 Vitaly. All rights reserved.
//


typealias MonsterStats = (
	hp : Int,
	dmg : Int,
	def_phys : Int,
	def_magic : Int,
	initiative : Int,
	mobility : Int,
	starting_act_pts : Int,
	crit : Int,
	dodge : Int,
	precision : Int,
	prierce : Int
)


typealias MonsterData = (
	id: Int,
	name: String,
	rarity: Int,
	attack_type: Int,
	damage_type: Int,
	portrait_name: String?,
	stats: [MonsterStats]
)


// bitfield used for filtering
struct MonsterRarityFilter : OptionSet {
	let rawValue: Int
	
	static let LEGENDARY  = MonsterRarityFilter(rawValue: 1 << 3)
	static let EPIC = MonsterRarityFilter(rawValue: 1 << 2)
	static let RARE  = MonsterRarityFilter(rawValue: 1 << 1)
	static let COMMON  = MonsterRarityFilter(rawValue: 1 << 0)
	
	static let ALL:MonsterRarityFilter = [.LEGENDARY,.EPIC,.RARE,.COMMON]
	static let NONE:MonsterRarityFilter = []
}
