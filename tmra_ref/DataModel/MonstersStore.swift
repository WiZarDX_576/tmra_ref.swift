//
//  MonstersStore.swift
//  tmra_ref
//
//  Created by Vitaly on 26.12.2018.
//  Copyright © 2018 Vitaly. All rights reserved.
//

import Foundation
import SQLite


enum DataAccessError: Error {
	case Datastore_Connection_Error
	case Insert_Error
	case Delete_Error
	case Search_Error
	case Nil_In_Data
}


// data access from sqlite DB

class MonstersDataStore {
	
	// db table names
	private struct TableNames {
		static let MONSTERS_DATA = "mercenary"
		static let MONSTER_STATS = "mercenary_level"
	}
	
	// monster data table binding
	private struct MonsterDataBinding {
		static let mon_id = Expression<Int>("id")
		static let name = Expression<String>("name")
		static let rarity = Expression<Int>("rarity")
		static let att_type = Expression<Int>("attack_type")
		static let dmg_type = Expression<Int>("damage_type")
		static let portrait_name = Expression<String?>("portrait")
	}
	
	// monster stats table binding
	private struct MonsterStatsDataBinding {
		static let mon_id = Expression<Int>("mercenary_id")
		static let level = Expression<Int>("level")
		static let skills = Expression<String>("skills")
		static let hp = Expression<Int>("hp")
		static let dmg = Expression<Int>("dmg")
		static let def_phys = Expression<Int>("def_phy")
		static let def_magic = Expression<Int>("def_mag")
		static let speed = Expression<Int>("speed")
		static let move = Expression<Int>("move")
		static let ap_init = Expression<Int>("ap_init")
		static let crit = Expression<Int>("crit")
		static let dodge = Expression<Int>("dodge")
		static let precision = Expression<Int>("hit")
		static let prierce = Expression<Int>("pierce_def")
	}
	
	// shortcuts for data binding
	private typealias MDB = MonsterDataBinding
	private typealias MSB = MonsterStatsDataBinding
	
	
	// db connection
	private let dbCnn: Connection?
	
	
	
	
	init() {
		let path = Bundle.main.path(forResource: "monstersDB", ofType: "sqlite")!
		
		do {
			dbCnn = try Connection(path)
		} catch _ {
			dbCnn = nil
		}
	}
	
	
	// data reading from SQL database
	
	func getMonsterFromDB(id: Int) throws -> MonsterData? {
		guard let DB = dbCnn else {
			throw DataAccessError.Datastore_Connection_Error
		}
		
		let table = Table(TableNames.MONSTERS_DATA)
		let query = table.filter(MDB.mon_id == id)
		
		let items = try DB.prepare(query)
		for item in items {
			let key_id = item[MDB.mon_id]
			let stats: [MonsterStats] = try getAllMonsterStatsFromDB(id: key_id)
			
			return MonsterData(
				id: key_id,
				name: item[MDB.name],
				rarity: item[MDB.rarity],
				attack_type: item[MDB.att_type],
				damage_type: item[MDB.dmg_type],
				portrait_name: item[MDB.portrait_name],
				stats)
		}
		
		return nil
	}
	
	
	func getAllMonsterStatsFromDB( id: Int ) throws -> [MonsterStats] {
		guard let DB = dbCnn else {
			throw DataAccessError.Datastore_Connection_Error
		}
		
		var resultsArray = [MonsterStats]()
		
		let table = Table(TableNames.MONSTER_STATS).filter(MSB.mon_id == id).order(MSB.level)
		let items = try DB.prepare(table)
		
		for item in items {
			resultsArray.append(MonsterStats(
				hp : item[MSB.hp],
				dmg : item[MSB.dmg],
				def_phys : item[MSB.def_phys],
				def_magic : item[MSB.def_magic],
				initiative : item[MSB.speed],
				mobility : item[MSB.move],
				starting_act_pts : item[MSB.ap_init],
				crit : item[MSB.crit],
				dodge : item[MSB.dodge],
				precision : item[MSB.precision],
				prierce : item[MSB.prierce]
			))
		}
		
		return resultsArray
	}
	
	func getAllMonstersFromDB() throws -> [MonsterData] {
		guard let DB = dbCnn else {
			throw DataAccessError.Datastore_Connection_Error
		}
		
		//var results:[Int:MonsterData] = [:]
		var results:[MonsterData] = []
		
		let table = Table(TableNames.MONSTERS_DATA)
		let items = try DB.prepare(table)
		
		for item in items {
			let key_id = item[MDB.mon_id]
			let stats: [MonsterStats] = try getAllMonsterStatsFromDB(id: key_id)
			
			results.append( MonsterData(
				id: key_id,
				name: item[MDB.name],
				rarity: item[MDB.rarity]-1,
				attack_type: item[MDB.att_type]-1,
				damage_type: item[MDB.dmg_type]-1,
				portrait_name: item[MDB.portrait_name],
				stats) )
		}
		
		return results
	}
	
}
