//
//  Constants.swift
//  tmra_ref
//
//  Created by Vitaly on 26.12.2018.
//  Copyright © 2018 Vitaly. All rights reserved.
//

import UIKit

struct MonsterConsts {
	static let RARITY_COUNT = 4
	
	static let RARITY_LEGENDARY = 3
	static let RARITY_EPIC = 2
	static let RARITY_RARE = 1
	static let RARITY_COMMON = 0
	
	static let RARITY_COLOR = [UIColor(red: 0/255, green: 192/255, blue: 0/255, alpha: 1.0), UIColor.blue, UIColor.purple, UIColor.orange]
	
	static let PORTRAIT_WIDTH:CGFloat = 150.0
	static let PORTRAIT_HEIGHT:CGFloat = 180.0
	static let PORTRAIT_ASPECT:CGFloat = PORTRAIT_WIDTH / PORTRAIT_HEIGHT
	static let PORTRAIT_UNKNOWN:String = "char_unknown"
	
	static let RARITY_NAME = ["Common","Rare","Epic","Legendary"]
	static let ATT_TYPE_NAME = ["Melee","Melee","Ranged","Ranged"]
	static let DMG_TYPE_NAME = ["Physical","Magic"]
}

typealias kMD = MonsterConsts

