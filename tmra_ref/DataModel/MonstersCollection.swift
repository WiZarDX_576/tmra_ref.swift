//
//  MonstersDB.swift
//  tmra_ref
//
//  Created by Vitaly on 23.12.2018.
//  Copyright © 2018 Vitaly. All rights reserved.
//

import Foundation
import UIKit


// class to keep monsters collection in memory
// made as singleton to allow any view controller query monster data

class MonstersCollection {
	static let sharedInstance = MonstersCollection()
	
	private var monstersDataDict:[Int:MonsterData] = [:]
	private var monstersIDsByRarity:[[Int]] = Array(repeating: [], count: kMD.RARITY_COUNT)
	
	
	func preloadData() {
		let store = MonstersDataStore()
		do {
			let monstersArray = try store.getAllMonstersFromDB()
			self.initWithData(monsters:monstersArray)
		} catch {
			// logging
		}
	}
	
	private func initWithData( monsters: [MonsterData] ) {
			for monster in monsters {
				monstersDataDict[monster.id] = monster
			}
			
			let keysArray = monstersDataDict.keys
			monstersIDsByRarity[kMD.RARITY_COMMON]		= keysArray.filter{$0 >= 100 && $0 <= 199}.sorted()
			monstersIDsByRarity[kMD.RARITY_RARE]		= keysArray.filter{$0 >= 200 && $0 <= 299}.sorted()
			monstersIDsByRarity[kMD.RARITY_EPIC]		= keysArray.filter{$0 >= 300 && $0 <= 399}.sorted()
			monstersIDsByRarity[kMD.RARITY_LEGENDARY]	= keysArray.filter{$0 >= 400 && $0 <= 499}.sorted()
	}
	
	
	// data access
	
	func getMonsterPortrait(id: Int) -> UIImage? {
		let imageName = monstersDataDict[id]?.portrait_name ?? kMD.PORTRAIT_UNKNOWN
		return UIImage(named: imageName)
	}
	
	
	func getMonsterByID(id: Int) -> MonsterData? {
		return monstersDataDict[id]
	}
	
	
	func getMonstersByRarityFilter(filter: MonsterRarityFilter) -> [MonsterData] {
		var resultList = [MonsterData]()
		
		if filter.contains(.LEGENDARY) {
			for i in monstersIDsByRarity[kMD.RARITY_LEGENDARY] {
				if let monsterData = getMonsterByID(id: i) {
					resultList.append( monsterData )
				}
			}
		}
		
		if filter.contains(.EPIC) {
			for i in monstersIDsByRarity[kMD.RARITY_EPIC] {
				if let monsterData = getMonsterByID(id: i) {
					resultList.append( monsterData )
				}
			}
		}
		
		if filter.contains(.RARE) {
			for i in monstersIDsByRarity[kMD.RARITY_RARE] {
				if let monsterData = getMonsterByID(id: i) {
					resultList.append( monsterData )
				}
			}
		}
		
		if filter.contains(.COMMON) {
			for i in monstersIDsByRarity[kMD.RARITY_COMMON] {
				if let monsterData = getMonsterByID(id: i) {
					resultList.append( monsterData )
				}
			}
		}
		
		return resultList
	}
	
}
