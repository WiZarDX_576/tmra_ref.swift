//
//  MonsterCollectionViewController.swift
//  tmra_ref
//
//  Created by Vitaly on 21.12.2018.
//  Copyright © 2018 Vitaly. All rights reserved.
//

import UIKit

private let reuseIdentifier = "MonsterColCell"


class MonsterCollectionViewCell: UICollectionViewCell {
	
	@IBOutlet weak var imgView: UIImageView!
	@IBOutlet weak var textField: UILabel!
	
	func initWith(monsterData: MonsterData, portraitImage: UIImage?) {
		self.layer.cornerRadius = 10
		self.layer.borderWidth = 3.0
		
		self.layer.borderColor = kMD.RARITY_COLOR[monsterData.rarity].cgColor
		self.layer.masksToBounds = true
		
		self.imgView.image = portraitImage
		self.textField.text = monsterData.name
	}
}


class MonsterCollectionViewController: UICollectionViewController, UICollectionViewDelegateFlowLayout {
	
	private var monstersList = [MonsterData]()
	
	
    override func viewDidLoad() {
        super.viewDidLoad()
		
		self.collectionView.allowsMultipleSelection = false
		self.collectionView.allowsSelection = true
		
		let filter = MonsterRarityFilter.ALL
		monstersList = MonstersCollection.sharedInstance.getMonstersByRarityFilter(filter: filter)
    }
	
	
    // MARK: UICollectionViewDataSource
	
    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }


    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return monstersList.count
    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as! MonsterCollectionViewCell
    
        // Configure the cell
		//let monsterData:MonsterData = MonstersDB.sharedInstance.getMonsterByID(id: monstersList[indexPath.row])
		let monsterData:MonsterData = monstersList[indexPath.row]
		let img = MonstersCollection.sharedInstance.getMonsterPortrait(id: monsterData.id)
		
		cell.initWith(monsterData: monsterData, portraitImage: img)
		
        return cell
    }
	
	
	// MARK: UICollectionViewDelegateFlowLayout
	
	public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
		let width = self.collectionView.frame.size.width / 3
		let height = width * (1.0 / kMD.PORTRAIT_ASPECT)
		
		return CGSize(width: width-2, height: height)
	}
	
	public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets{
		
		return UIEdgeInsets(top: 1, left: 1, bottom: 1, right: 1)
	}
	
	public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat{
		
		return 1
	}
	
	public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat{
		
		return 1
	}
	

    // MARK: UICollectionViewDelegate
	
	override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
		let popOverVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "MonsterDetailsPopup") as! MonsterDetailsController
		self.addChild(popOverVC)
		popOverVC.view.frame = self.view.frame
		
		popOverVC.showInView(aView: self.view, monsterData: monstersList[indexPath.row], animated: true)
		popOverVC.didMove(toParent: self)
	}

    /*
    // Uncomment this method to specify if the specified item should be highlighted during tracking
    override func collectionView(_ collectionView: UICollectionView, shouldHighlightItemAt indexPath: IndexPath) -> Bool {
        return true
    }
    */

    /*
    // Uncomment this method to specify if the specified item should be selected
    override func collectionView(_ collectionView: UICollectionView, shouldSelectItemAt indexPath: IndexPath) -> Bool {
        return true
    }
    */

    /*
    // Uncomment these methods to specify if an action menu should be displayed for the specified item, and react to actions performed on the item
    override func collectionView(_ collectionView: UICollectionView, shouldShowMenuForItemAt indexPath: IndexPath) -> Bool {
        return false
    }

    override func collectionView(_ collectionView: UICollectionView, canPerformAction action: Selector, forItemAt indexPath: IndexPath, withSender sender: Any?) -> Bool {
        return false
    }

    override func collectionView(_ collectionView: UICollectionView, performAction action: Selector, forItemAt indexPath: IndexPath, withSender sender: Any?) {
    
    }
    */

}
