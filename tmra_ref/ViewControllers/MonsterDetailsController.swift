//
//  MonsterDetailsController.swift
//  tmra_ref
//
//  Created by Vitaly on 22.12.2018.
//  Copyright © 2018 Vitaly. All rights reserved.
//

import UIKit

class MonsterStatViewCell: UICollectionViewCell {
	
	@IBOutlet weak var imgView: UIImageView!
	@IBOutlet weak var textField: UILabel!
	
	func initWith(statImage: UIImage, statValue: String) {
		self.layer.cornerRadius = 2
		//self.layer.borderWidth = 3.0
		self.layer.masksToBounds = true
		
		//self.contentView.translatesAutoresizingMaskIntoConstraints = false
		//self.textField.preferredMaxLayoutWidth = 25
		
		self.imgView.image = statImage
		self.textField.text = statValue
	}
}


class MonsterDetailsController: UIViewController, UIGestureRecognizerDelegate, UICollectionViewDataSource, UIPickerViewDelegate, UIPickerViewDataSource {
	@IBOutlet weak var closeButton: UIButton!
	@IBOutlet weak var popUpView: UIView!
	@IBOutlet weak var portraitImg: UIImageView!
	@IBOutlet weak var monsterName: UILabel!
	@IBOutlet weak var rarityText: UILabel!
	@IBOutlet weak var attTypeText: UILabel!
	@IBOutlet weak var dmgTypeText: UILabel!
	
	@IBOutlet weak var statsCollectionView: UICollectionView!
	@IBOutlet weak var levelPickerView: UIPickerView!
	
	
	
	// TODO - rework to use icons cache here
	private typealias PreparedStat = ( icon : UIImage, text : String )
	
	private var monsterData: MonsterData?
	private var preparedStats = [PreparedStat]()
	private var animated = false
	
	
    override func viewDidLoad() {
        super.viewDidLoad()

		self.view.backgroundColor = UIColor.black.withAlphaComponent(0.9)
		popUpView.layer.cornerRadius = 5
		popUpView.layer.backgroundColor = UIColor.white.cgColor
		popUpView.layer.masksToBounds = true
		
		closeButton.layer.cornerRadius = closeButton.frame.height / 2
		closeButton.layer.masksToBounds = true
		
		let recog : UITapGestureRecognizer = UITapGestureRecognizer.init(target: self, action:#selector(HandleTap))
		recog.numberOfTapsRequired = 1
		//recog.numberOfTouchesRequired = 1
		recog.cancelsTouchesInView = true
		recog.delegate = self
		self.view.addGestureRecognizer(recog)
		
		statsCollectionView.dataSource = self
		levelPickerView.dataSource = self
		levelPickerView.delegate = self
    }
	
	func shutdown() {
		self.view.removeFromSuperview()
		self.didMove(toParent: nil)
		self.removeFromParent()
	}
	
	func showInView(aView: UIView!, monsterData : MonsterData, animated: Bool)
	{
		self.monsterData = monsterData
		
		let img = MonstersCollection.sharedInstance.getMonsterPortrait(id: monsterData.id)
		portraitImg.image = img
		monsterName.text = monsterData.name
		rarityText.text = kMD.RARITY_NAME[monsterData.rarity]
		attTypeText.text = kMD.ATT_TYPE_NAME[monsterData.attack_type]
		dmgTypeText.text = kMD.DMG_TYPE_NAME[monsterData.damage_type]
		
		self.prepareTextForStats(stats: monsterData.stats[0])
		self.statsCollectionView.reloadData()
		
		aView.addSubview(self.view)
		self.animated = animated
		
		if animated {
			self.showAnimate()
		}
	}
	
	func showAnimate()
	{
		self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
		self.view.alpha = 0.0;
		UIView.animate(withDuration: 0.25, animations: {
			self.view.alpha = 1.0
			self.view.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
		});
	}
	
	func removeAnimate()
	{
		UIView.animate(withDuration: 0.25, animations: {
			self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
			self.view.alpha = 0.0;
		}, completion:{(finished : Bool) in
			if (finished) {
				self.shutdown()
			}
		});
	}
	
	@IBAction func closePopup(sender: AnyObject) {
		if self.animated {
			self.removeAnimate()
		} else {
			self.shutdown()
		}
	}
	
	
	@objc func HandleTap(sender:UITapGestureRecognizer) -> Void
	{
		if sender.state == UIGestureRecognizer.State.ended{
			let location = sender.location(in: self.popUpView)
			
			if !popUpView.bounds.contains(location) {
				self.view.removeGestureRecognizer(sender)
				closePopup(sender:sender)
			}
		}
	}
	
	//*
	func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
		return true
	}//*/
	
	
	// MARK: UICollectionViewDataSource
	
	func numberOfSections(in collectionView: UICollectionView) -> Int {
		return 1
	}
	
	func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
		return self.preparedStats.count
	}
	
	func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
		let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "StatCell", for: indexPath) as! MonsterStatViewCell
		
		// Configure the cell
		cell.initWith(statImage: self.preparedStats[indexPath.item].icon, statValue: self.preparedStats[indexPath.item].text)
		
		return cell
	}
	
	// MARK: UIPickerViewDataSource
	
	func numberOfComponents(in pickerView: UIPickerView) -> Int {
		return 1
	}
	
	func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
		return 23
	}
	
	// MARK: UIPickerViewDelegate
	
	func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
		return String(row+1)
	}
	
	func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent  component: Int) {
		if let md = self.monsterData {
			self.prepareTextForStats(stats: md.stats[row])
			self.statsCollectionView.reloadData()
		}
	}
	
	
	private func prepareTextForStats( stats: MonsterStats ) {
		self.preparedStats.removeAll()
		preparedStats.append(PreparedStat(icon: UIImage(named: "icon_attr_hp")!, text: String(stats.hp)))
		preparedStats.append(PreparedStat(icon: UIImage(named: "icon_attr_dmg")!, text: String(stats.dmg)))
		preparedStats.append(PreparedStat(icon: UIImage(named: "icon_attr_def_phy")!, text: String(stats.def_phys)))
		preparedStats.append(PreparedStat(icon: UIImage(named: "icon_attr_def_mag")!, text: String(stats.def_magic)))
		preparedStats.append(PreparedStat(icon: UIImage(named: "icon_attr_speed")!, text: String(stats.initiative)))
		preparedStats.append(PreparedStat(icon: UIImage(named: "icon_attr_move")!, text: String(stats.mobility)))
		
		if(stats.crit > 0) {
			preparedStats.append(PreparedStat(icon: UIImage(named: "icon_attr_crit")!, text: String(stats.crit)))
		}
		
		if(stats.precision > 0) {
			preparedStats.append(PreparedStat(icon: UIImage(named: "icon_attr_hit")!, text: String(stats.precision)))
		}
		
		if(stats.starting_act_pts > 0) {
			preparedStats.append(PreparedStat(icon: UIImage(named: "icon_attr_ap_init")!, text: String(stats.starting_act_pts)))
		}
		
		if(stats.dodge > 0) {
			preparedStats.append(PreparedStat(icon: UIImage(named: "icon_attr_dodge")!, text: String(stats.dodge)))
		}
	}
	
	
}



extension UILabel {
	func initWith(image: UIImage, with text: String) {
		let attachment = NSTextAttachment()
		attachment.image = image
		attachment.bounds = CGRect(x: 0, y: -5, width: self.frame.height, height: self.frame.height)
		let attachmentStr = NSAttributedString(attachment: attachment)
		
		let result = NSMutableAttributedString()
		result.append(attachmentStr)
		
		let textString = NSAttributedString(string: text, attributes: [.font: self.font])
		result.append(textString)
		
		self.attributedText = result
	}
}
